from django.apps import AppConfig


class TaskyAppConfig(AppConfig):
    name = 'taskpy_app'
