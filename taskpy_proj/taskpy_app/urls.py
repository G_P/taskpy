from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name = 'index'),
    path('dtime/', views.current_datetime, name='current_datetime'),

]

