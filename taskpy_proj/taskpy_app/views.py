from django.shortcuts import render
from django.http import HttpResponse
import datetime


# Create your views here.
def index(request):
    return HttpResponse('Home page')


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)


def home(request):
    my_dict = {'insert_me':'i am from taskpy.app/views'}
    return render(request, 'home.html', context=my_dict)
